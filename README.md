# MovieRama #

MovieRama is a Spring Boot - Web Application
that you can search a movie or see the movies currently playing in cinemas.

App is currently up and running on [Heroku](https://ps-movierama.herokuapp.com/)

## To Deploy ##

1. mvn package
2. java -jar target/movierama-0.0.1-SNAPSHOT.jar

Default port is 8080