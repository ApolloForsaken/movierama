package siatos.movierama.models;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * The search object
 * @author Petros Siatos
 *
 */
public class SearchDto {

	@NotEmpty
	private String query;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
	
}
