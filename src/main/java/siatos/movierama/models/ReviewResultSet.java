package siatos.movierama.models;

import java.util.HashSet;
import java.util.Set;

/**
 * the interface for a result set from a Movie Apis response.
 * For requesting the reviews of a movie.
 * @author Petros Siatos
 *
 * @param <T>
 */
public class ReviewResultSet implements ResultSet<Review> {
	
	@Override
	public Set<Review> getResults() {
		return new HashSet<>();
	}

	@Override
	public int getPage() {
		return 0;
	}

	@Override
	public int getTotalResults() {
		return 0;
	}

	@Override
	public int getTotalPages() {
		return 0;
	}
}