package siatos.movierama.models;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * the interface for a result set after a call to a Movie Api.
 * @author Petros Siatos
 *
 * @param <T>
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public interface ResultSet<T> {

	public int getPage();
	public Set<T> getResults();
	public int getTotalResults();
	public int getTotalPages();
	
}
