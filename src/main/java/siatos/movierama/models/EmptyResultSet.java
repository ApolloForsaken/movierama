package siatos.movierama.models;

import java.util.HashSet;
import java.util.Set;

/**
 * A class defining an empty Result Set
 * @author Petros Siatos
 *
 */
public final class EmptyResultSet<T extends Object> implements ResultSet<T> {

	@Override
	public int getPage() {
		return 0;
	}

	@Override
	public Set<T> getResults() {
		return new HashSet<>();
	}

	@Override
	public int getTotalResults() {
		return 0;
	}

	@Override
	public int getTotalPages() {
		return 0;
	}
	
}