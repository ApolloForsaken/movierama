package siatos.movierama.models.tmdb;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import siatos.movierama.models.Actor;
import siatos.movierama.models.Movie;

public class TheMovieDatabaseMovie extends Movie {

	private String title = "";
	
	private String description = "";
	
	// TODO: Change to java8 LocalDateTime, (add Custom Converter?) 
	private String releaseDate;
	
	private int totalReviews = 0;
	
	@Override
	public String getTitle() {
		return title;
	}

	// title and not original title because it will not be the english one if it's a foreign movie.
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@JsonProperty("overview")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	@JsonProperty("release_date")
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Override
	public int getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(int totalReviews) {
		this.totalReviews = totalReviews;
	}

	@Override
	public Set<Actor> getActors() {
		return new HashSet<>();
	}

	@Override
	public String getProductionYear() {
		// TODO: Fix the bad implementation
		if (releaseDate.length() >= 4)
			return releaseDate.substring(0, 4);
		else
			return "";
	}
	
}
