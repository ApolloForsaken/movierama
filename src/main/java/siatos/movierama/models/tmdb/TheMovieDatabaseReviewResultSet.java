package siatos.movierama.models.tmdb;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TheMovieDatabaseReviewResultSet extends TheMovieDatabaseResultSet<TheMovieDatabaseReview> {

	private Set<TheMovieDatabaseReview> reviews = new HashSet<>();
	
	public Set<TheMovieDatabaseReview> getReviews() {
		return reviews;
	}

	@JsonProperty("results")
	public void setReviews(Set<TheMovieDatabaseReview> reviews) {
		this.reviews = reviews;
	}

	@Override
	public Set<TheMovieDatabaseReview> getResults() {
		return reviews;
	}

}
