package siatos.movierama.models.tmdb;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TheMovieDatabaseMovieResultSet extends TheMovieDatabaseResultSet<TheMovieDatabaseMovie> {

	private Set<TheMovieDatabaseMovie> movies = new HashSet<>();
	
	public Set<TheMovieDatabaseMovie> getMovies() {
		return movies;
	}

	@JsonProperty("results")
	public void setMovies(Set<TheMovieDatabaseMovie> movies) {
		this.movies = movies;
	}

	@Override
	public Set<TheMovieDatabaseMovie> getResults() {
		return movies;
	}

}
