package siatos.movierama.models;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The model for a movie
 * @author Petros Siatos
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public abstract class Movie implements Comparable<Movie> {

	private long id;
	
	public abstract String getTitle();
	
	public abstract String getDescription();

	public abstract String getProductionYear();

	public abstract int getTotalReviews();

	public abstract Set<Actor> getActors();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * Returns true if both movies have same title and same production year.
	 * @param movie
	 * @return
	 */
	public boolean equals(Object other) {
		
		if (other == null) return false;
		if (!(other instanceof Movie)) return false; 
		
		Movie movie = (Movie) other;
			
		return (this.getTitle().equalsIgnoreCase(movie.getTitle())
				&&
				this.getProductionYear().equals(movie.getProductionYear()));
	}

	@Override
	public int hashCode() {
		
		//TODO: see caching
		int result = 42;
		result = 31 * result + getProductionYear().hashCode();
		result = 31 * result + getTitle().hashCode();
		
		return result;
	}

	@Override
	public int compareTo(Movie other) {
		return (this.getTitle().compareTo(other.getTitle()) +
				(this.getProductionYear().compareTo(other.getProductionYear())));
	}

	@Override
	public String toString() {
		return getTitle() + " - " + getProductionYear() + " - " + getDescription() + " - " + getActors();
	}
	
}
