package siatos.movierama.models.rt;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RottenTomatoReviewResultSet extends RottenTomatoResultSet<RottenTomatoReview> {

	private Set<RottenTomatoReview> reviews = new HashSet<>();

	public Set<RottenTomatoReview> getReviews() {
		return reviews;
	}

	@JsonProperty("reviews")
	public void setReviews(Set<RottenTomatoReview> reviews) {
		this.reviews = reviews;
	}

	@Override
	public Set<RottenTomatoReview> getResults() {
		return reviews;
	}
	
}
