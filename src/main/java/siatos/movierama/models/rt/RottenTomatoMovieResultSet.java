package siatos.movierama.models.rt;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RottenTomatoMovieResultSet extends RottenTomatoResultSet<RottenTomatoMovie> {

	private Set<RottenTomatoMovie> movies = new HashSet<>();
	
	public Set<RottenTomatoMovie> getMovies() {
		return movies;
	}

	@JsonProperty("movies")
	public void setMovies(Set<RottenTomatoMovie> movies) {
		this.movies = movies;
	}

	@Override
	public Set<RottenTomatoMovie> getResults() {
		return movies;
	}
	
}
