package siatos.movierama.models.rt;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

import siatos.movierama.models.Actor;
import siatos.movierama.models.Movie;

public class RottenTomatoMovie extends Movie {
	
	private String title = "";
	
	private String description = "";
	
	private String year;
	
	private int totalReviews = 0;
	
	private Set<Actor> actors = new HashSet<>();
	
	private URI reviewsUri;

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@JsonProperty("synopsis")
	public void setDescription(String description) {
		this.description = description;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Override
	public int getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(int totalReviews) {
		this.totalReviews = totalReviews;
	}

	@Override
	public Set<Actor> getActors() {
		return actors;
	}

	@JsonProperty("abridged_cast")
	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	public URI getReviewsUri() {
		return reviewsUri;
	}

	public void setReviewsUri(URI reviewsUri) {
		this.reviewsUri = reviewsUri;
	}

	@Override
	public String getProductionYear() {
		return year;
	}
	
}
