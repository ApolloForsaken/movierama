package siatos.movierama.models.rt;

import com.fasterxml.jackson.annotation.JsonProperty;

import siatos.movierama.models.ResultSet;

public abstract class RottenTomatoResultSet<T> implements ResultSet<T> {

	private int page;
	
	private int totalResults;
	
	private int totalPages;

	@Override
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public int getTotalResults() {
		return totalResults;
	}
	
	@JsonProperty("total")
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	@Override
	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

}
