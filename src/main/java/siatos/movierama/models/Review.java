package siatos.movierama.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A review about a movie
 * @author Petros Siatos
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public abstract class Review {
}
