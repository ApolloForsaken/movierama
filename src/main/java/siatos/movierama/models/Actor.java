package siatos.movierama.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The model for an actor
 * @author Petros Siatos
 *
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class Actor {

	private String name;
	
	public Actor() {
		super();	
	}
	
	public Actor(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
	
}
