package siatos.movierama.models;

import java.util.HashSet;
import java.util.Set;

/**
 * A simple implementation of the {@link Movie} abstract class
 * @author Petros Siatos
 *
 */
public class SimpleMovie extends Movie {

	private String title = "";
	
	private String description = "";
	
	private String productionYear = "";
	
	private int totalReviews;
	
	private Set<Actor> actors = new HashSet<>();

	/**
	 * Constructor merging two movies to one.
	 * @param a
	 * @param b
	 */
	public SimpleMovie(Movie a, Movie b) {
		totalReviews =  a.getTotalReviews() + b.getTotalReviews();
		title = a.getTitle();
		productionYear = a.getProductionYear();
				
		if (a.getDescription().length() >= b.getDescription().length())
			description = a.getDescription();
		else
			description = b.getDescription();
			
		actors = a.getActors();
		actors.addAll(b.getActors());	
	}
	
	private SimpleMovie(Builder builder) {
		super();
		this.title = builder.title;
		this.description = builder.description;
		this.productionYear = builder.productionYear;
		this.totalReviews = builder.totalReviews;
		this.actors.addAll(builder.actors);
	}

	@Override
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(String productionYear) {
		this.productionYear = productionYear;
	}

	@Override
	public int getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(int totalReviews) {
		this.totalReviews = totalReviews;
	}

	@Override
	public Set<Actor> getActors() {
		return actors;
	}

	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}
	
	public static class Builder {
		
		// Required params
		private final String title;
		
		private final String productionYear;
		
		// Optional params
		private String description;
		
		private int totalReviews;
		
		private Set<Actor> actors = new HashSet<>();

		public Builder(String title, String productionYear) {
			super();
			this.title = title;
			this.productionYear = productionYear;
		}
		
		public Builder description(String description) {
			this.description = description;
			return this;
		}
		
		public Builder totalReviews(int totalReviews) {
			this.totalReviews = totalReviews;
			return this;
		}
		
		public Builder actors(Set<Actor> actors) {
			this.actors.addAll(actors);
			return this;
		}
		
		public SimpleMovie build() {
			return new SimpleMovie(this);
		}
		
	}
	
}
