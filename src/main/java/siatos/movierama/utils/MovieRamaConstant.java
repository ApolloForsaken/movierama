package siatos.movierama.utils;

/**
 * Constant values for MovieRama application
 * @author Petros Siatos
 *
 */
public class MovieRamaConstant {

	// TODO: To be moved to property file.
	
	// Rotten Tomatoes (RT)
	public static String RT_API_KEY = "qtqep7qydngcc7grk4r4hyd9";
	
	public static String RT_SCHEME				= "http";
	public static String RT_HOST	  			= "api.rottentomatoes.com";
	public static String RT_SEARCH_PATH 		= "api/public/v1.0/movies.json";
	public static String RT_NOW_PLAYING_PATH	= "api/public/v1.0/lists/movies/in_theaters.json";
	public static String RT_REVIEWS_PATH		= "api/public/v1.0/movies/{id}/reviews.json";
	
	public static String RT_KEY_API = "apikey";
	public static String RT_KEY_QUERY = "q";
	
	// The Movie Database (TMDB)
	public static String TMDB_API_KEY = "186b266209c2da50f898b7977e2a44dd";
	
	public static String TMDB_SCHEME			= "https";
	public static String TMDB_HOST	  			= "api.themoviedb.org";
	public static String TMDB_SEARCH_PATH 		= "3/search/movie";
	public static String TMDB_NOW_PLAYING_PATH 	= "3/movie/now_playing";
	public static String TMDB_REVIEWS_PATH 		= "3/movie/{id}/reviews";
		
	
	public static String TMDB_KEY_API = "api_key";
	public static String TMDB_KEY_QUERY = "query";
	
}
