package siatos.movierama;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Spring Boot - MovieRama Application
 * @author Petros Siatos
 *
 */
@SpringBootApplication
public class MovieRamaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRamaApplication.class, args);
	}

}
