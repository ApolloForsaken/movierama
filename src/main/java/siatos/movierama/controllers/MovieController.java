package siatos.movierama.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import siatos.movierama.models.SearchDto;
import siatos.movierama.services.MovieMergeService;

@Controller
@RequestMapping(path = "/")
public class MovieController {

	@Autowired
	private MovieMergeService mergeService;
	
	@ModelAttribute
	public SearchDto searchDto() {
		return new SearchDto();
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView nowPlaying(ModelAndView mnv) {
		
		mnv.addObject("movies", mergeService.nowPlaying());
		
		mnv.setViewName("home");
		return mnv;
	}
	
	@RequestMapping(path = "/search", method = RequestMethod.POST)
	public ModelAndView search(@Valid @ModelAttribute SearchDto searchDto, BindingResult result, ModelAndView mnv) {
		System.out.println("Query: " + searchDto.getQuery());
		
		if (!result.hasErrors()) {
			mnv.addObject("movies", mergeService.search(searchDto.getQuery()));
		}
		
		mnv.setViewName("home");
		return mnv;
	}
	
}