package siatos.movierama.services;

import static siatos.movierama.utils.MovieRamaConstant.TMDB_API_KEY;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_HOST;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_KEY_API;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_KEY_QUERY;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_NOW_PLAYING_PATH;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_REVIEWS_PATH;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_SCHEME;
import static siatos.movierama.utils.MovieRamaConstant.TMDB_SEARCH_PATH;

import java.net.URI;
import java.util.Set;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import siatos.movierama.models.EmptyResultSet;
import siatos.movierama.models.ResultSet;
import siatos.movierama.models.tmdb.TheMovieDatabaseMovie;
import siatos.movierama.models.tmdb.TheMovieDatabaseMovieResultSet;
import siatos.movierama.models.tmdb.TheMovieDatabaseReview;
import siatos.movierama.models.tmdb.TheMovieDatabaseReviewResultSet;

public class TheMovieDatabaseService implements MovieService<TheMovieDatabaseMovie, TheMovieDatabaseReview> {

	private RestTemplate restTemplate;
	
	public TheMovieDatabaseService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Override
	public ResultSet<TheMovieDatabaseMovie> getSearchResultSet(String query) {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add(TMDB_KEY_API, TMDB_API_KEY);
		params.add(TMDB_KEY_QUERY, query);
		
		ResultSet<TheMovieDatabaseMovie> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getUri(TMDB_SEARCH_PATH, params), TheMovieDatabaseMovieResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("TMDB - SEARCH GONE WRONG");
			ex.printStackTrace();
		}
	
		return resultSet;	
	}
	
	@Override
	public ResultSet<TheMovieDatabaseMovie> getNowPlayingResultSet() {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add(TMDB_KEY_API, TMDB_API_KEY);
		
		ResultSet<TheMovieDatabaseMovie> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getUri(TMDB_NOW_PLAYING_PATH, params), TheMovieDatabaseMovieResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("TMDB - NOW PLAYING GONE WRONG");
			ex.printStackTrace();
		}
	
		return resultSet;
	}
	
	@Override
	public ResultSet<TheMovieDatabaseReview> getReviewsResultSet(Long movieId) {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add(TMDB_KEY_API, TMDB_API_KEY);
		
		
		ResultSet<TheMovieDatabaseReview> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getTemplateUri(TMDB_REVIEWS_PATH, movieId, params), TheMovieDatabaseReviewResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("TMDB - NOW PLAYING GONE WRONG");
			System.out.println("TMDB - Reviews for Movie: " + movieId);
			System.out.println(getTemplateUri(TMDB_REVIEWS_PATH, movieId,params));
		}
	
		return resultSet;
	}
	
	@Override
	public Set<TheMovieDatabaseMovie> search(String query) {
		ResultSet<TheMovieDatabaseMovie> resultSet = getSearchResultSet(query);
		resultSet.getTotalPages();
		
		resultSet.getResults()
			.parallelStream()
			.forEach(movie -> movie.setTotalReviews(reviews(movie.getId())));
		
		return resultSet.getResults();
	}

	@Override
	public Set<TheMovieDatabaseMovie> nowPlaying() {
		ResultSet<TheMovieDatabaseMovie> resultSet = getNowPlayingResultSet();
		resultSet.getTotalPages();
		
		resultSet.getResults()
			.parallelStream()
			.forEach(movie -> movie.setTotalReviews(reviews(movie.getId())));
		
		return resultSet.getResults();
	}
	
	@Override
	public int reviews(Long movieId) {
		return getReviewsResultSet(movieId).getTotalResults();
	}
	
	public URI getUri(String path, MultiValueMap<String,String> params) {
		return 	UriComponentsBuilder.newInstance()
					.scheme(TMDB_SCHEME)
					.host(TMDB_HOST)
					.path(path)
					.queryParams(params)
					.build()
					.encode().toUri();
	}
	
	public URI getTemplateUri(String path, Object pathParam,MultiValueMap<String,String> params) {
		return 	UriComponentsBuilder.newInstance()
					.scheme(TMDB_SCHEME)
					.host(TMDB_HOST)
					.path(path)
					.queryParams(params)
					.buildAndExpand(pathParam)
					.encode().toUri();
	}

}
