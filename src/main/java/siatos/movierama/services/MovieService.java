package siatos.movierama.services;

import java.util.Set;

import siatos.movierama.models.Movie;
import siatos.movierama.models.ResultSet;
import siatos.movierama.models.Review;

/**
 * The interface for a service that includes movie calls to a movie api
 * @author Petros Siatos
 *
 * @param <T>
 * @param <R>
 */
public interface MovieService<T extends Movie, R extends Review> {

	public ResultSet<T> getSearchResultSet(String query);
	public ResultSet<T> getNowPlayingResultSet();
	public ResultSet<R> getReviewsResultSet(Long movieId);
	
	public Set<T>		search(String query);
	public Set<T>		nowPlaying();
	public int			reviews(Long movieId);
	
}
