package siatos.movierama.services;

import static siatos.movierama.utils.MovieRamaConstant.RT_API_KEY;
import static siatos.movierama.utils.MovieRamaConstant.RT_HOST;
import static siatos.movierama.utils.MovieRamaConstant.RT_KEY_API;
import static siatos.movierama.utils.MovieRamaConstant.RT_KEY_QUERY;
import static siatos.movierama.utils.MovieRamaConstant.RT_SCHEME;
import static siatos.movierama.utils.MovieRamaConstant.RT_SEARCH_PATH;
import static siatos.movierama.utils.MovieRamaConstant.RT_NOW_PLAYING_PATH;
import static siatos.movierama.utils.MovieRamaConstant.RT_REVIEWS_PATH;

import java.net.URI;
import java.util.Set;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import siatos.movierama.models.EmptyResultSet;
import siatos.movierama.models.ResultSet;
import siatos.movierama.models.rt.RottenTomatoMovie;
import siatos.movierama.models.rt.RottenTomatoMovieResultSet;
import siatos.movierama.models.rt.RottenTomatoReview;
import siatos.movierama.models.rt.RottenTomatoReviewResultSet;

public class RottenTomatoService implements MovieService<RottenTomatoMovie, RottenTomatoReview> {

	private RestTemplate restTemplate;
	
	public RottenTomatoService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public ResultSet<RottenTomatoMovie> getSearchResultSet(String query) {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

		params.add(RT_KEY_API, RT_API_KEY);
		params.add(RT_KEY_QUERY, query);

		ResultSet<RottenTomatoMovie> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getUri(RT_SEARCH_PATH, params), RottenTomatoMovieResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("RT - QUERY GONE WRONG");
			System.out.println(query);
			ex.printStackTrace();
		}
		
		return resultSet;
	}
	
	@Override
	public ResultSet<RottenTomatoMovie> getNowPlayingResultSet() {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add(RT_KEY_API, RT_API_KEY);
		
		ResultSet<RottenTomatoMovie> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getUri(RT_NOW_PLAYING_PATH, params), RottenTomatoMovieResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("RT - NOW PLAYING GONE WRONG");
			ex.printStackTrace();
		}
	
		return resultSet;
	}
	
	@Override
	public ResultSet<RottenTomatoReview> getReviewsResultSet(Long movieId) {
		
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		
		params.add(RT_KEY_API, RT_API_KEY);
		
		ResultSet<RottenTomatoReview> resultSet = new EmptyResultSet<>();
		
		try {
			resultSet = restTemplate.getForObject(getTemplateUri(RT_REVIEWS_PATH, movieId,params), RottenTomatoReviewResultSet.class);
		} catch(RestClientException ex) {
			
			System.out.println("RT - GET REVIEWS GONE WRONG");
			System.out.println("RT - Reviews for Movie: " + movieId);
			System.out.println(getTemplateUri(RT_REVIEWS_PATH, movieId,params));
		}
	
		return resultSet;
	}
	
	@Override
	public Set<RottenTomatoMovie> search(String query) {
		ResultSet<RottenTomatoMovie> resultSet = getSearchResultSet(query);
		resultSet.getTotalPages();
		
		resultSet.getResults()
			.parallelStream()
			.forEach(movie -> movie.setTotalReviews(reviews(movie.getId())));
		
		return resultSet.getResults();
	}

	@Override
	public Set<RottenTomatoMovie> nowPlaying() {
		ResultSet<RottenTomatoMovie> resultSet = getNowPlayingResultSet();
		resultSet.getTotalPages();
		
		resultSet.getResults()
			.parallelStream()
			.forEach(movie -> movie.setTotalReviews(reviews(movie.getId())));
		
		return resultSet.getResults();
	}
	
	@Override
	public int reviews(Long movieId) {
		return getReviewsResultSet(movieId).getTotalResults();
	}

	// TODO: remove duplication
	public URI getUri(String path, MultiValueMap<String, String> params) {
		return UriComponentsBuilder.newInstance()
				.scheme(RT_SCHEME)
				.host(RT_HOST)
				.path(path)
				.queryParams(params)
				.build()
				.encode()
				.toUri();
	}
	

	public URI getTemplateUri(String path, Object pathParam,MultiValueMap<String,String> params) {
		return 	UriComponentsBuilder.newInstance()
				.scheme(RT_SCHEME)
				.host(RT_HOST)
				.path(path)
				.queryParams(params)
				.buildAndExpand(pathParam)
				.encode().toUri();
	}


}
