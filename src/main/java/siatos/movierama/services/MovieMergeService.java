package siatos.movierama.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import siatos.movierama.config.MovieRamaConfig;
import siatos.movierama.models.Movie;
import siatos.movierama.models.SimpleMovie;
import siatos.movierama.models.rt.RottenTomatoMovie;
import siatos.movierama.models.rt.RottenTomatoReview;
import siatos.movierama.models.tmdb.TheMovieDatabaseMovie;
import siatos.movierama.models.tmdb.TheMovieDatabaseReview;

/**
 * The Service that merges results from both apis
 * @author Petros Siatos
 *
 */
@Service
public class MovieMergeService {

	private static final ExecutorService threadPool = Executors.newWorkStealingPool();
	
	@Autowired
	private MovieService<RottenTomatoMovie, RottenTomatoReview> rtService;
	
	@Autowired
	private MovieService<TheMovieDatabaseMovie, TheMovieDatabaseReview> tmdbService;
	
	@Cacheable(MovieRamaConfig.CACHE_SEARCH_NOVIES)
	public Set<Movie> search(String query) {
		
		// SERIAL
//		long start_time = System.nanoTime();
//		
//		List<Movie> result = mergeDuplicateMovies(serialSearch(query));
//		
//		System.out.println(" SERIAL HITTER ");
//		
//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time)/1e6;
//		System.out.println(difference);
		
		// CONCURRENT
//		long start_time = System.nanoTime();
		
		Set<Movie> result = mergeDuplicateMovies(concurrentSearch(query));
		
//		System.out.println(" SEARCH - CONCURRENT HITTER ");
//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time)/1e6;
//		System.out.println(difference);
		
		return result;
	}
	
	@Cacheable(MovieRamaConfig.CACHE_NOW_PLAYING_MOVIES)
	public Set<Movie> nowPlaying() {
		
		// CONCURRENT
//		long start_time = System.nanoTime();
		
		Set<Movie> result = mergeDuplicateMovies(concurrentNowPlaying());
		
//		System.out.println(" NOW PLAYING - CONCURRENT HITTER ");
//		long end_time = System.nanoTime();
//		double difference = (end_time - start_time)/1e6;
//		System.out.println(difference);
		
		return result;
	}
	
	public List<Movie> serialSearch(String query) {
		
//		Set<TheMovieDatabaseMovie> tmdbResults = tmdbService.search(query);
//		tmdbResults.getMovies().forEach(System.out::println);
//		Set<RottenTomatoMovie> rtResults = rtService.search(query);
//		rtResults.getMovies().forEach(System.out::println);
		
		List<Movie> movies = new ArrayList<>(); 
		movies.addAll(tmdbService.search(query));
		movies.addAll(rtService.search(query));
		
		return movies;
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Movie> concurrentNowPlaying() {
		
		List<Callable<Set<?>>> callables = Arrays.asList(
				() -> rtService.nowPlaying(),
				() -> tmdbService.nowPlaying()
		);
		
		return (List<Movie>) concurrentGetResults(callables);
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<Movie> concurrentSearch(String query) {
	
		List<Callable<Set<?>>> callables = Arrays.asList(
				() -> rtService.search(query),
				() -> tmdbService.search(query)
		);
		
		return (List<Movie>) concurrentGetResults(callables);
	}
	
	/**
	 * A function that invokes all callables and returns a list with all the results collected
	 * @param callables
	 * @return
	 */
	// TODO: Fix the Generics issue.
	public static List<?> concurrentGetResults(List<Callable<Set<?>>> callables) {
		
		List<?> results = new ArrayList<>();
		
		try {
			results = threadPool.invokeAll(callables)
			    .stream()
			    .map(future -> {
			        try {
			            return future.get();
			        }
			        catch (Exception e) {
			        	e.printStackTrace();
			        	return new HashSet<>();
//			            throw new IllegalStateException(e);
			        }
			    })
			    .flatMap(Set::stream)
			    .collect(Collectors.toList());
		} catch (InterruptedException e) {
			System.out.println("concurrentGetResults was interrupted.");
			e.printStackTrace();
		}
		
		return results;
	}
	
	/**
	 * A different version of concurrentGetResults using Java8 Stream Api
	 * @param callables
	 * @return
	 */
	public static List<?> parallelStreamGetResults(List<Callable<Set<?>>> callables) {
		List<?> results = new ArrayList<>();
		
		try {
			results = callables
				.parallelStream()
				.map(callable -> {
					try {
						return callable.call();
					} catch (Exception e) {
						
						e.printStackTrace();
						return new HashSet<>();
					}
				})
				.flatMap(Set::stream)
				.collect(Collectors.toList());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return results;
	}
	
	
	/**
	 * Merge duplicates in a list of movies
	 * Assumes there are at most 2 duplicates of each movie. 
	 * @param movies
	 * @return
	 */
	public Set<Movie> mergeDuplicateMovies(List<Movie> movies) {
		
		Set<Movie> result = new HashSet<>(); 
		
		// if list is empty return empty set
		if (movies.isEmpty()) return result;
		
		// Sort list to have same movie in consecutive positions.
		Iterator<Movie> iter = movies.stream()
				.sorted()
				.iterator();
		
		// We already know there is at least one element.
		Movie previous = iter.next();
		
		while (iter.hasNext()) {
			
			Movie next = iter.next();
			
			// If they are equals merge them (set previous to null so it's not added twice.)
			if (next.equals(previous)) {
				result.add(new SimpleMovie(previous, next));
				previous = null;
			} else {
				if (previous != null) result.add(previous);
				previous = next;
			}
		}
		
		if (previous != null) result.add(previous);
		
		return result;
	}
	
}
