package siatos.movierama.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import siatos.movierama.services.TheMovieDatabaseService;

@Configuration
public class TheMovieDatabaseConfig {

	@Bean
	public TheMovieDatabaseService theMovieDatabaseService() {
		
		RestTemplate restTemplate = new RestTemplate();
		
		return new TheMovieDatabaseService(restTemplate);
	}
	
}
