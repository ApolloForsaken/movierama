package siatos.movierama.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.Cache;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.google.common.cache.CacheBuilder;

/**
 * Configuration for movierama application including caches and message source
 * @author Petros Siatos
 *
 */
@Configuration
@Import({ RottenTomatoConfig.class, TheMovieDatabaseConfig.class })
@ComponentScan({ MovieRamaConfig.PACKAGE_ROOT })
@EnableCaching
public class MovieRamaConfig {

	public static final String PACKAGE_ROOT = "siatos.movierama";

	public final static String CACHE_SEARCH_NOVIES = "search-movies";
	public final static String CACHE_NOW_PLAYING_MOVIES = "now-playing-movies";

	// Message Resources
	@Bean(name = "messageSource")
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("properties/messages");
		return messageSource;
	}
	
	@Bean
	public Cache search() {
		return new GuavaCache(CACHE_SEARCH_NOVIES, CacheBuilder.newBuilder()
				.expireAfterWrite(7, TimeUnit.DAYS)
				.build());
	}
	
	@Bean
	public Cache nowPlaying() {
		return new GuavaCache(CACHE_NOW_PLAYING_MOVIES, CacheBuilder.newBuilder()
				.expireAfterWrite(7, TimeUnit.DAYS)
				.build());
	}
}
