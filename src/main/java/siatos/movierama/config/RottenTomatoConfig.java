package siatos.movierama.config;

import java.io.IOException;
import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

import siatos.movierama.services.RottenTomatoService;

@Configuration
public class RottenTomatoConfig {


	@Bean
	public RottenTomatoService rottenTomatoService() {
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setInterceptors(Arrays.asList(new SetJsonContentTypeResponseInterceptor()));
		
		return new RottenTomatoService(restTemplate);
	}
	
	/**
	 * Interceptor to change the faulty response content type from "text/javascript" to "application/json" that Rotten Tomatoes api returns.
	 * @author Petros Siatos
	 *
	 */
	private static class SetJsonContentTypeResponseInterceptor implements ClientHttpRequestInterceptor {

		@Override
		public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
				throws IOException {	
			ClientHttpResponse response = execution.execute(request, body);
			response.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);
			
			return response;
		}

	}
}
