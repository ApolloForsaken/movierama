package siatos.movierama.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import siatos.movierama.config.MovieRamaConfig;
import siatos.movierama.models.Actor;
import siatos.movierama.models.Movie;
import siatos.movierama.models.SimpleMovie;
import siatos.movierama.services.MovieMergeService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MovieRamaConfig.class)
public class MovieMergeServiceTest {
	
	@Autowired
	private MovieMergeService movieMergeService;
	
	private List<Movie> moviesToBeMerged;
	
	private Set<Actor> actors;
	
	@Before
	public void setup() {
		
		actors = new HashSet<>(Arrays.asList(new Actor("Jason"), new Actor("Ryan"), new Actor("Petros")));
		
		moviesToBeMerged = new ArrayList<>();
		
		moviesToBeMerged.add(new SimpleMovie.Builder("Terminator", "1984").description("Katitis").totalReviews(25).build());
		moviesToBeMerged.add(new SimpleMovie.Builder("Terminator", "1984").description("Katitis Parapanw").totalReviews(10).actors(actors).build());
		
	}
	
	@Test
	public void mergeMovies() {
		Set<Movie> merged = movieMergeService.mergeDuplicateMovies(moviesToBeMerged);
		
		assertEquals(1, merged.size());
		
		Movie terminator = merged.iterator().next();
		
		assertEquals("Terminator", terminator.getTitle());
		assertEquals("Katitis Parapanw", terminator.getDescription());
		assertEquals("1984", terminator.getProductionYear());
		assertEquals(35, terminator.getTotalReviews());
		assertEquals(actors, terminator.getActors());
		
	}
	
	
}
