package siatos.movierama.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import siatos.movierama.models.Movie;
import siatos.movierama.models.SimpleMovie;
import siatos.movierama.services.MovieMergeService;

public class MovieControllerTest {
	
	@Mock
	private MovieMergeService movieService;
	
	@InjectMocks
	private MovieController movieController;
	
	private MockMvc mockMvc;
	
	private Set<Movie> terminators;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		mockMvc = MockMvcBuilders
				.standaloneSetup(movieController)
				.build();
		
		terminators = new HashSet<>();
		
		terminators.add(new SimpleMovie.Builder("Terminator", "1984").build());
		terminators.add(new SimpleMovie.Builder("Terminator 2 - Judgement Day", "1991").build());
		terminators.add(new SimpleMovie.Builder("Terminator 3 - Salvation", "2009").build());
		terminators.add(new SimpleMovie.Builder("Terminator 4 - Genisys", "2015").build());
//		
	}
	
	@Test
	public void searchTerminator() throws Exception {
		
		when(movieService.search("Terminator")).thenReturn(terminators);
		
		mockMvc.perform(post("/search")
			.param("query", "Terminator"))
			.andExpect(status().isOk())
			.andExpect(view().name("home"))
			.andExpect(model().attribute("movies", terminators));
	}
	
}
