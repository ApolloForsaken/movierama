package siatos.movierama.other;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class UriBuilderTest {

	@Test
	public void constructUriSpring() {
		
		String api_key = "186b266209c2da50f898b7977e2a44dd";
		String query = "The Big Lebowski";
		
		UriComponents uriComponents =
			UriComponentsBuilder.newInstance()
				.scheme("https")
				.host("api.themoviedb.org")
				.path("3/search/movie")
				.queryParam("api_key", api_key)
				.queryParam("query", query)
				.build()
				.encode();

		assertEquals("https://api.themoviedb.org/3/search/movie?api_key=186b266209c2da50f898b7977e2a44dd&query=The%20Big%20Lebowski", uriComponents.toUriString());
	}
	
	@Test
	public void constructUriSpringWithTemplate() {
		
		UriComponents uriComponents =
				UriComponentsBuilder.newInstance()
					.scheme("http")
					.host("api.rottentomatoes.com")
					.path("api/public/v1.0/movies/{id}/reviews.json")
					.buildAndExpand("770672122")
					.encode();
		
		assertEquals("http://api.rottentomatoes.com/api/public/v1.0/movies/770672122/reviews.json", uriComponents.toUriString());
		
	}

}
