package siatos.movierama.other;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

public class IteratorTest {

	public class A {
		
		private int a;
		private int b;
		
		public A(int a, int b) {
			this.a = a;
			this.b = b;
		}

		public int getA() {
			return a;
		}

		public int getB() {
			return b;
		}
		
		public boolean test(A other) {
			
			if (other == null) return false;
			return (this.a == other.a) && (this.b == other.b);
		}

		@Override
		public String toString() {
			return a + " - " + b;
		}

	};
	
	@Test
	public void testingIterators() {
		
		A a = new A(1,3);
		A b = new A(2,4);
		A c = new A(2,4);
		A d = new A(3,1);
		A e = new A(3,1);
		A f = new A(4,3);
		A g = new A(4,4);

		List<A> z = Arrays.asList(a, b, c, d, e, f, g);
		
		List<A> mergeList = new ArrayList<>();
		
		Iterator<A> iter = z.iterator();
		
		A previous = iter.next();
		
		while (iter.hasNext()) {
			A next = iter.next();
			
			if (previous.test(next)) {
				
				mergeList.add(previous);
				mergeList.add(next);
				
				System.out.println("Merge: " + previous + " with " + next);
			}
			
			previous = next;
		}
		
		assertEquals(Arrays.asList(b,c,d,e), mergeList);
		
	}
	
}
