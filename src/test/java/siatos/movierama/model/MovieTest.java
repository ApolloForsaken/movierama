package siatos.movierama.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;

import siatos.movierama.models.Movie;
import siatos.movierama.models.rt.RottenTomatoMovie;
import siatos.movierama.models.tmdb.TheMovieDatabaseMovie;

public class MovieTest {

	@Test
	public void compareTest() {
		RottenTomatoMovie rt1 = new RottenTomatoMovie();
		rt1.setTitle("Terminator 2");
		rt1.setYear("1999");
		
		RottenTomatoMovie rt2 = new RottenTomatoMovie();
		rt2.setTitle("Terminator 2");
		rt2.setYear("2010");
		
		RottenTomatoMovie rt3 = new RottenTomatoMovie();
		rt3.setTitle("Armegogeladwn");
		rt3.setYear("2010");
		
		TheMovieDatabaseMovie tmdb = new TheMovieDatabaseMovie();
		tmdb.setTitle("Terminator 2");
		tmdb.setReleaseDate("1999");
		
		List<Movie> movies = Arrays.asList(rt1, rt2, rt3, tmdb);
		
		List<Movie> sortedMovies;
		
		sortedMovies = movies.stream().sorted().collect(Collectors.toList());
		
		Assert.assertEquals(Arrays.asList(rt3, rt1, tmdb, rt2), sortedMovies);
		Assert.assertEquals(rt1,  tmdb);
		Assert.assertNotEquals(rt1,  rt2);
		Assert.assertNotEquals(rt2,  tmdb);
	}
	
}
